import moment from "moment-timezone";
import { formatOffset, getToday, isAmPm, getTZOffset } from "@/utils/time";

export const state = () => ({
  date: null,
  hover: {},
  amPm: null,
  timezone: null
});

export const getters = {
  date: state => {
    return state.date ? state.date : getToday();
  },
  isAmPm: state =>
    typeof state.isAmPm === "boolean" ? state.isAmPm : isAmPm(),
  formatTime: (state, getters) => (
    timestamp,
    timezone = getters.currentTimeZone
  ) => {
    const ts = moment.tz(timestamp, timezone);
    return state.amPm ? ts.format("hh:mm A") : ts.format("HH:mm");
  },
  availableTimezones: (state, getters, rootState) =>
    rootState.team
      .reduce((all, curr) => {
        if (!all.includes(curr.timezone)) {
          all.push(curr.timezone);
        }
        return all;
      }, [])
      .sort(),
  hoverState: (state, getters) => {
    const { posX } = state.hover;

    if (posX) {
      return {
        localTime: getters.formatTime(getters.currentTime),
        timestamp: getters.currentTime.format(),
        posX: posX,
        class: "hover"
      };
    }
    return {};
  },
  currentTime: (state, getters) => {
    let { minutes = 0, timestamp } = state.hover;

    if (!timestamp) {
      timestamp = moment.tz(getters.currentTimeZone);
      timestamp = getters.date + "T" + timestamp.format("HH:mm");
    }

    return moment
      .tz(timestamp, getters.currentTimeZone)
      .add(minutes, "minutes");
  },
  currentTimeZone: state =>
    typeof state.timezone === "string" ? state.timezone : moment.tz.guess(true),
  currentOffset: (state, getters) =>
    getTZOffset(getters.currentTimeZone, getters.date),
  groupedByOffset: (state, getters, rootState) => {
    const grouped = rootState.team.reduce(
      (all, curr) => {
        const timezone = curr.timezone;

        if (!timezone || rootState.hidden.includes(curr.slug)) {
          return all;
        }

        const offset = getTZOffset(timezone, getters.date);

        if (!all[offset]) {
          all[offset] = {
            offset,
            tanukis: [],
            timezones: [],
            order: offset
          };
        }

        if (!all[offset].timezones.includes(timezone)) {
          !all[offset].timezones.push(timezone);
        }

        all[offset].tanukis.push(curr);
        return all;
      },
      {
        [getters.currentOffset]: {
          offset: getters.currentOffset,
          tanukis: [],
          timezones: [getters.currentTimeZone],
          order: -1000
        }
      }
    );

    return Object.values(grouped)
      .map(({ timezones, ...rest }) => ({
        ...rest,
        timezones: timezones.sort()
      }))
      .sort((a, b) => a.order - b.order);
  },
  timeSlots: (state, getters) => {
    if (!getters.groupedByOffset || getters.groupedByOffset.length === 0) {
      return [];
    }

    const offsets = getters.groupedByOffset.map(x => x.offset);

    const date = moment(getters.date).format("YYYY-MM-DD");

    const minOffset = Math.min(...offsets);
    const maxOffset = Math.max(...offsets);

    const startDate = getRoundedHour(
      moment(date + "T05:00:00" + formatOffset(minOffset))
    );

    const endDate = getRoundedHour(
      moment(date + "T23:00:00" + formatOffset(maxOffset))
    );

    const allDate = [];

    do {
      allDate.push(startDate.format());
      startDate.add(1, "h");
    } while (startDate.isSameOrBefore(endDate));

    return allDate;
  }
};

const getRoundedHour = date => {
  const newMoment = moment(date);
  const offset = newMoment.utcOffset();
  newMoment.add(Math.abs(offset % 60), "minutes");
  return newMoment;
};

export const mutations = {
  updateField(state, message) {
    for (let key in message) {
      state[key] = message[key];
    }
  }
};
