import moment from "moment-timezone";

const padZeros = value => {
  return value.toString().padStart(2, "0");
};

const AM_PM_REGEX = /am|pm/i;

export const isAmPm = () => {
  const now = new Date();

  return (
    AM_PM_REGEX.test(now.toLocaleTimeString()) ||
    AM_PM_REGEX.test(now.toString())
  );
};

export const getTZOffset = (TZ, date) =>
  moment.tz.zone(TZ).utcOffset(moment(date));

export const getToday = () => {
  const now = new Date();

  return (
    now.getFullYear() +
    "-" +
    padZeros(now.getMonth() + 1) +
    "-" +
    padZeros(now.getDate())
  );
};

export const formatOffset = offset => {
  const hours = Math.abs(Math.floor(offset / 60));
  const minutes = Math.abs(offset % 60);
  return (offset <= 0 ? "+" : "-") + padZeros(hours) + ":" + padZeros(minutes);
};
